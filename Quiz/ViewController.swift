//
//  ViewController.swift
//  Quiz
//
//  Created by Connor Burk on 7/19/17.
//  Copyright © 2017 Connor Burk. All rights reserved.
//

import UIKit

class ViewController: UIViewController {
    
    override func viewDidLoad() {
        super.viewDidLoad()
        questionLabel.text = questions[currentQuestionIndex]
    }


    @IBOutlet var questionLabel: UILabel!

    @IBOutlet var answerLabel: UILabel!
    
    // typed code ( arrays and index defined )
    let questions: [String] = ["From what is cognac made?", "What is 7+7?", "What is the capital of Vermont"]
    
    let answers: [String] = ["Grapes", "14", "Montpelier"]
    
    var currentQuestionIndex: Int = 0
    
    // End of typed code
    
    @IBAction func showNextQuestion(_ sender: Any) {
        
        currentQuestionIndex += 1
        if currentQuestionIndex == questions.count {
            currentQuestionIndex = 0
        }
        
        let question: String = questions[currentQuestionIndex]
        questionLabel.text = question
        answerLabel.text = "???"
        
    }
    
    @IBAction func showAnswer(_ sender: Any) {
    
        let answer: String = answers[currentQuestionIndex]
        answerLabel.text = answer
        
    }
    
}

